Oracle Endeca Information Discovery

Who is better to explain something than it cretor?The following brief description it's taken from the official documentation of the Oracle Endeca Information Discovery

Oracle Endeca Information Discovery is a complete solution for agile
data discovery across the enterprise, empowering business user 
innovation in balance with IT governance and control.

It consists of three core components:

- Endeca Information Discovery Studio: an application that allows business users to easily upload and mash up multiple diverse data sources and configure discovery applications.
It includes world-class search, guided navigation, filtering, and an array of powerful interactive visualizations for rapid analysis that requires zero training.

For an overview of this app see: https://www.youtube.com/watch?v=xUUolAPcEvQ&index=4&list=PLQl6jp6EE5H2vnx8wCxZOpkwIIf8tUZjk

- Endeca Server: an hybrid search-analytical engine that organizes complex and varied data from disparate sources.

With Endeca Server's unique NoSQL-like data model and inmemory architecture, Endeca Information Discovery create an extremely agile framework for handling complex data combinations. It also supports 35 distinct languages.

For more information see: https://www.youtube.com/watch?v=Hl84LBDs4XE&list=PLQl6jp6EE5H2vnx8wCxZOpkwIIf8tUZjk&index=2

- Endeca Information Discovery Integrator: is a powerful visual data integration environment that includes the Information Acquisition System for gathering content from file systems, content management systems, and websites.

All the documentation for Endeca Information Discovery and all the versions is in: http://www.oracle.com/technetwork/middleware/endeca/documentation/index.html


