import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
 
import sklearn.cluster
from sklearn import metrics
from sklearn import neighbors
import numpy as np
from sklearn.externals.six import StringIO
import pydot  

import random as r

def createNewDataCard(workroads_per_zone):
	
	file = open("Results2006.xml","w")
	file.write('<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>\n')
	file.write('<root>\n')
	tree = ET.parse('DataCard2006.xml')
	root = tree.getroot()
	i=0
	for child in root:
		file.write('\t<Zona>\n')
		for element in child:
			#print(element.tag,element.text)
			file.write('\t\t<'+element.tag+'>'+element.text+'</'+element.tag+'>\n')
		#print("Obras: ",workroads_per_zone[i])
		file.write('\t\t<Obras>'+str(workroads_per_zone[i])+'</Obras>\n')
		file.write('\t</Zona>\n')
		i+=1
	file.write('</root>')
	file.close()

def main():
	#Process
	tree = ET.parse('TrainingData.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	zonas=[]
	for child in root:
		longitudes.append(float(child[12].text))
		latitudes.append(float(child[13].text))
		zonas.append(int(child[14].text))

	data=[]
	for i in zip(longitudes,latitudes,zonas):
		data.append(i)
	
	#Training and Test data
	training=[]
	test=[]

	for aux in data:
		if r.random()>0.6:
			test.append(aux)
		else:
			training.append(aux)

	#Training
	TrainingX=[] #Longitud, Latitud
	TrainingY=[] #zona
	for aux in training:
		TrainingX.append([aux[0],aux[1]])
		TrainingY.append(aux[2])
	
	#Test
	TestX=[]
	TestY=[]
	for aux in test:
		TestX.append([aux[0],aux[1]])
		TestY.append(aux[2])


	percent=[]
	for weights in ['uniform', 'distance']:
		for k in range(1,15):
			clf = neighbors.KNeighborsClassifier(k, weights=weights)
			clf.fit(TrainingX, TrainingY)
			Z = clf.predict(TrainingX)
			score = clf.score(TrainingX,TrainingY)
			percent.append([k,score])

	#Show the better K-value
	k_value=0
	aux=0
	for k,value in percent:
		if value>aux:
			aux=value
			k_value=k
	
	#Now we hace the better k-value we do the knn with this k-value
	tree = ET.parse('TrainingData.xml')
	root = tree.getroot()
	X = []
	Y = []
	for child in root:
		X.append([float(child[12].text),float(child[13].text)])
		Y.append(int(child[14].text))

	clf = neighbors.KNeighborsClassifier(k, weights=weights)
	clf.fit(X, Y)
	Z = clf.predict(X)

	print("===RESULT KNN 2007===")
	print("======Workroads======")

	tree = ET.parse('datos2007.xml')
	root = tree.getroot()
	X_Workroads=[]
	for child in root:
		if child[0].text == "Obras":
			if child[12].text != None and child[13].text != None:
				X_Workroads.append([float(child[12].text),float(child[13].text)])
	#Now execute the KNN
	clf = neighbors.KNeighborsClassifier(k, weights=weights)
	clf.fit(X, Y)
	Z = clf.predict(X_Workroads)
	#Count the number of accidents for each zone
	Workroads_per_zone={}
	for aux in Z:
		if aux in Workroads_per_zone.keys():
			Workroads_per_zone[aux]+=1
		else:
			Workroads_per_zone[aux]=1
	print("Zone: number of Workroads")
	print(Workroads_per_zone)
	createNewDataCard(Workroads_per_zone)

	#Now we make a decission tree

	#Process
	tree = ET.parse('Results2006.xml')
	root = tree.getroot()
	trainingX = []
	trainingY = []
	feature_names=[]
	target_names=[]
	i = 0
	for child in root:
		target_names.append(str(i))
		i+=1
		
	for child in root:
		data = []
		#Number of accidents
		trainingY.append(int(child[8].text))
		for aux in child:
			data.append(int(aux.text))
		trainingX.append(data)

	for aux in root[0]:
		feature_names.append(aux.tag)

			

	#feature_names=["N_accidentes","Alcances","Atropellos","Salida","Tijera_Camion","Vuelco","Accidentes_Diurnos","Accidentes_Nocturnos"]
	#target_names=["1","2","3","4","5","6","7","8","9","10","11","12","13"]

	#Training the data
	from sklearn import tree
	clftree = tree.DecisionTreeClassifier(criterion='entropy',max_depth=3, random_state=0)
	clftree.fit(trainingX, trainingY)

	# Extract the decision tree logic from the trained model
	dot_data = StringIO() 
	tree.export_graphviz(clftree, out_file=dot_data,feature_names=feature_names, class_names=target_names, filled=True, rounded=True, special_characters=True)

	# convert the logics into graph
	graph = pydot.graph_from_dot_data(dot_data.getvalue()) 

	## This will plot decision tree in pdf file
	graph.write_pdf(path="tree.pdf")

if __name__ == "__main__":
	main()