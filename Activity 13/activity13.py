import xml.etree.ElementTree as ET
from sklearn.externals.six import StringIO
import pydot  

def main():
	#Process
	tree = ET.parse('DataCard.xml')
	root = tree.getroot()
	trainingX = []
	trainingY = []
	for child in root:
		data = []
		#Number of accidents
		trainingY.append(int(child[0].text))
		for aux in child:
			data.append(int(aux.text))
		trainingX.append(data)

	print(trainingX)
	print(trainingY)
	feature_names=["N_accidentes","Alcances","Atropellos","Salida","Tijera_Camion","Vuelco","Accidentes_Diurnos","Accidentes_Nocturnos"]
	target_names=["1","2","3","4","5","6","7","8","9","10","11","12","13"]

	#Training the data
	from sklearn import tree
	clftree = tree.DecisionTreeClassifier(criterion='entropy',max_depth=3, random_state=0)
	clftree.fit(trainingX, trainingY)

	# Extract the decision tree logic from the trained model
	dot_data = StringIO() 
	tree.export_graphviz(clftree, out_file=dot_data,feature_names=feature_names, class_names=target_names, filled=True, rounded=True, special_characters=True)

	# convert the logics into graph
	graph = pydot.graph_from_dot_data(dot_data.getvalue()) 

	## This will plot decision tree in pdf file
	graph.write_pdf(path="tree.pdf")


if __name__ == "__main__":
	main()