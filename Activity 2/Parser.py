import xml.etree.ElementTree as ET

def main():
	#Open the xml file and initialize the result file
	file = open("result.xml","w")
	file.write('<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>\n')
	file.write('<root>\n')
	tree = ET.parse('IncidenciasTDTHist.xml')
	root = tree.getroot()
	cont=0
	#Reading of the xml file and writing the result file
	for child in root:
		for aux in child:
			if aux.tag == 'provincia' and ( aux.text == 'GIPUZKOA' or aux.text =='Gipuzkoa'):
				cont+=1
				file.write('\t<Incidencia>\n')
				for item in child:
					if item.text =='Gipuzkoa':
						file.write("\t\t<"+str(item.tag)+">"+"GIPUZKOA"+"</"+str(item.tag)+">\n")
					else:	
						file.write("\t\t<"+str(item.tag)+">"+str(item.text)+"</"+str(item.tag)+">\n")
				file.write('\t</Incidencia>\n')
	file.write('</root>')
	file.close()
	print(cont)

if __name__ =="__main__":
	main()
