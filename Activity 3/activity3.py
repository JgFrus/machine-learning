import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET

from sklearn.cluster import DBSCAN 
import sklearn.cluster
from sklearn import metrics
from sklearn.preprocessing import StandardScaler
import sklearn.neighbors
from sklearn.neighbors import kneighbors_graph
import numpy


#Methods for showing the data
#Obtained from the scripts of Francisco Pascual
def plotdata(data,labels,name): #def function plotdata
#colors = ['black']
    fig, ax = plt.subplots()
    plt.scatter([row[0] for row in data], [row[1] for row in data], c=labels)
    ax.grid(True)
    fig.tight_layout()
    plt.title(name)
    #Save Result as a Image
    fig1 = plt.gcf()
    plt.show()
    fig1.savefig('Result.png', dpi=100)

def main():
	#Process the xml
	tree = ET.parse('result.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	valid=False
	for child in root:
		for aux in child:
			if aux.text == 'Accidente':
				valid=True
			if aux.tag == 'longitud' and aux.text != "None" and valid ==True:
				longitudes.append(float(aux.text))
			if aux.tag == 'latitud' and aux.text != "None" and valid ==True:
				latitudes.append(float(aux.text))
				valid=False

	print("Number of elemets= "+str(len(longitudes)))
	

	#1. Transform the data in a single list for the clustering methods 
	data=[]
	for i  in range(0,len(longitudes)):
		data.append([longitudes[i],latitudes[i]])
	labels = [0 for x in range(len(data))]
	
	#Getting the best parameters
	dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
	
	matsim = dist.pairwise(data)
	minPts=10 #Between 5 and 10 works fine

	A = kneighbors_graph(data, minPts, include_self=False)
	Ar = A.toarray()

	seq = [] #For the eps parameter
	for i,s in enumerate(data):
		for j in range(len(data)):
			if Ar[i][j] != 0:
				seq.append(matsim[i][j])
	seq.sort()
	plt.plot(seq)
	plt.show()

	# 2. Execute clustering (dbscan)
	labels = sklearn.cluster.DBSCAN(eps=0.005, min_samples=minPts).fit_predict(data)
	n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
	print("Number of Clusters= "+str(n_clusters))

	# 3. Plot the results
	plotdata(data,labels, 'dbscan')

	# 4. Validation
	print("Silhouette Coefficient: %0.3f"% metrics.silhouette_score(numpy.asarray(data), labels))


if __name__ == "__main__":
	main()