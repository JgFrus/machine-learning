import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
 
import sklearn.cluster
from sklearn import metrics
import numpy as np


def plotdata(data,labels,name): #def function plotdata
#colors = ['black']
    fig, ax = plt.subplots()
    plt.scatter([row[0] for row in data], [row[1] for row in data], c=labels)
    ax.grid(True)
    fig.tight_layout()
    plt.title(name)
    #Save Result as a Image
    fig1 = plt.gcf()
    plt.show()
    fig1.savefig(name+'.png', dpi=100)

def main():
	#Process
	tree = ET.parse('result.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	valid=False
	for child in root:
		for aux in child:
			if aux.text == 'Accidente':
				valid=True
			if aux.tag == 'longitud' and aux.text != "None" and valid ==True:
				longitudes.append(float(aux.text))
			if aux.tag == 'latitud' and aux.text != "None" and valid ==True:
				latitudes.append(float(aux.text))
				valid=False

	#1.Convert the data in a single list 
	data=[]
	for i  in range(0,len(longitudes)):
		data.append([longitudes[i],latitudes[i]])
	labels = [0 for x in range(len(data))]


	#Number of clusters obtained from the graphs.py script. Same number of clusters for K-means and EM.
	k=13
	init = "k-means++"

	#2. Calculate and plot de K-means results, including Silhouette
	centroids, labels, z =  sklearn.cluster.k_means(data, k, init)
	plotdata(data,labels, 'K-means')
	print("Silhouette Coefficient: %0.3f"  % metrics.silhouette_score(np.asarray(data), labels))

	#5. Calculate and plot the GMM results.
	from sklearn.mixture import GMM
	classifier = GMM(n_components=13,covariance_type='full', init_params='wc', n_iter=20)
	classifier.fit(data)
	labels =  classifier.predict(data)
	plotdata(data,labels,'EM')
	print("Silhouette Coefficient (EM): %0.3f" % metrics.silhouette_score(np.asarray(data), labels))

	#6. Calculate and plot the Spectral Clustering results    
	spectral = sklearn.cluster.SpectralClustering(n_clusters=13, eigen_solver='arpack', affinity="nearest_neighbors")
	labels = spectral.fit_predict(data)
	plotdata(data,labels,'Spectral CLustering')
	print("Silhouette Coefficient (Spectral): %0.3f"% metrics.silhouette_score(np.asarray(data), labels))


if __name__ == "__main__":
	main()