import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
from sklearn.cluster import KMeans
import sklearn.cluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import sklearn.neighbors
from sklearn.neighbors import kneighbors_graph
import numpy as np


def plotdata(data,labels,name): #def function plotdata
#colors = ['black']
    fig, ax = plt.subplots()
    plt.scatter([row[0] for row in data], [row[1] for row in data], c=labels)
    ax.grid(True)
    fig.tight_layout()
    plt.title(name)
    #Save Result as a Image
    fig1 = plt.gcf()
    plt.show()
    fig1.savefig('Result.png', dpi=100)

#In this script, we show the graphs of the distortion and silhouettes for chose 
#the best number of clusters in k-means
def main():
	#Process
	tree = ET.parse('result.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	valid=False
	for child in root:
		for aux in child:
			if aux.text == 'Accidente':
				valid=True
			if aux.tag == 'longitud' and aux.text != "None" and valid ==True:
				longitudes.append(float(aux.text))
			if aux.tag == 'latitud' and aux.text != "None" and valid ==True:
				latitudes.append(float(aux.text))
				valid=False

	print("Number of elemets= "+str(len(longitudes)))

	#Convert in a single list 
	data=[]
	for i  in range(0,len(longitudes)):
		data.append([longitudes[i],latitudes[i]])
	labels = [0 for x in range(len(data))]

	# 2. Setting parameters (ad-hoc)

	# parameters
	init = 'random' # initialization method 
	iterations = 10 # to run 10 times with different random centroids to choose the final model as the one with the lowest SSE
	max_iter = 300 # maximum number of iterations for each single run
	tol = 1e-04 # controls the tolerance with regard to the changes in the within-cluster sum-squared-error to declare convergence
	random_state = 0 # random


	distortions = []
	silhouettes = []

	for i in range(2, 21):
	    km = KMeans(i, init, n_init = iterations ,max_iter= max_iter, tol = tol,random_state = random_state)
	    labels = km.fit_predict(data)
	    distortions.append(km.inertia_)
	    silhouettes.append(metrics.silhouette_score(np.asarray(data), labels))

	# Plot distoritions    
	plt.plot(range(2,21), distortions, marker='o')
	plt.xlabel('Number of clusters')
	plt.ylabel('Distortion')
	plt.show()

	# Plot Silhouette
	plt.plot(range(2,21), silhouettes , marker='o')
	plt.xlabel('Number of clusters')
	plt.ylabel('Silohouette')
	plt.show()

if __name__ == "__main__":
	main()