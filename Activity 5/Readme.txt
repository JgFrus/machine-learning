In this activity we have to group the accidents by zone and add features to this zones. We do it on the "FiltradoZona.py" script, where we add to each accident, its zone.

After that, on the "Seleccion.py" script, we add the features for each zone. We choose the number of accidents, the number of each type of accident, and the number of nocturne and diurn accidents.

Finally, with the data card obtained, we apply the PCA.
