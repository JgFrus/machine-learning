import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET

import numpy as np


def main():
	#Process
	tree = ET.parse('FilteredResult.xml')
	root = tree.getroot()

	#Number of zones	
	n=0
	for child in root:
		zone = int(child[14].text)
		if zone > n:
			n=zone

	#Data almacena [zona,diccionario(atributo:n)]
	data = []
	for i in range(0,n+1):
		info={'Accidentes' : 0}
		data.append([i,info])

	#Search accidents by their zone
	for child in root:
		zone = int(child[14].text)
		data[zone][1]["Accidentes"]+=1
	
	#Dictionary
	new_data = {} #Cluster -> Nodos
	for i in range(0,n+1):
		nodes=[]
		for child in root:
			zone = int(child[14].text)
			if zone == i:
				nodes.append(child)
		new_data[i]=nodes

	
	file = open("DataCard.xml","w")
	file.write('<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>\n')
	file.write('<root>\n')

	dic_tipos_acci = {}

	for i in range(0,n+1): 
		file.write('\t<Zona>\n')
		diurno=0
		nocturno=0
		for aux in new_data[i]:

			#Para saber si es un accidente nocturno o diurno
			if int(aux[6].text[11:13]) >= 8:
				diurno += 1
			else:
				nocturno += 1


			#Tipo Accidente
			if aux[4].text in dic_tipos_acci.keys():#Se comprueba si existe ya
				dic_tipos_acci[aux[4].text]+=1
			else:
				dic_tipos_acci[aux[4].text]=1

			
		file.write('\t\t<N_accidentes>'+str(data[i][1]["Accidentes"])+'</N_accidentes>\n')
		
		if 'Alcance' in dic_tipos_acci.keys():
			file.write('\t\t<Alcances>'+str(dic_tipos_acci['Alcance'])+'</Alcances>\n')
		else:
			file.write('\t\t<Alcances>'+'0'+'</Alcances>\n')

		if 'Atropello' in dic_tipos_acci.keys():
			file.write('\t\t<Atropellos>'+str(dic_tipos_acci['Atropello'])+'</Atropellos>\n')
		else:
			file.write('\t\t<Atropellos>'+'0'+'</Atropellos>\n')

		if 'Salida' in dic_tipos_acci.keys():
			file.write('\t\t<Salida>'+str(dic_tipos_acci['Salida'])+'</Salida>\n')
		else:
			file.write('\t\t<Salida>'+'0'+'</Salida>\n')

		if 'Tijera camiÃ³n' in dic_tipos_acci.keys():
			file.write('\t\t<Tijera_Camion>'+str(dic_tipos_acci['Tijera camiÃ³n'])+'</Tijera_Camion>\n')
		else:
			file.write('\t\t<Tijera_Camion>'+'0'+'</Tijera_Camion>\n')

		if 'Vuelco' in dic_tipos_acci.keys():
			file.write('\t\t<Vuelco>'+str(dic_tipos_acci['Vuelco'])+'</Vuelco>\n')
		else:
			file.write('\t\t<Vuelco>'+'0'+'</Vuelco>\n')	
		
		file.write('\t\t<Accidentes_Diurnos>'+str(diurno)+'</Accidentes_Diurnos>\n')
		file.write('\t\t<Accidentes_Nocturnos>'+str(nocturno)+'</Accidentes_Nocturnos>\n')

		file.write('\t</Zona>\n')
		#Limpiar diccionarios
		dic_tipos_acci.clear()
		

	file.write('</root>')
	file.close()




		

	


if __name__ == "__main__":
	main()