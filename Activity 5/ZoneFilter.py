import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET

import sklearn.cluster


def main():
	#Process
	tree = ET.parse('result.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	valid=False
	for child in root:
		for aux in child:
			if aux.text == 'Accidente':
				valid=True
			if aux.tag == 'longitud' and aux.text != "None" and valid ==True:
				longitudes.append(float(aux.text))
			if aux.tag == 'latitud' and aux.text != "None" and valid ==True:
				latitudes.append(float(aux.text))
				valid=False

	data=[]
	for i  in range(0,len(longitudes)):
		data.append([longitudes[i],latitudes[i]])
	labels = [0 for x in range(len(data))]


	#Number of clusters
	k=13
	init = "k-means++"
	
	centroids, labels, z =  sklearn.cluster.k_means(data, k, init)

	#Creo el nuevo archivo XML con su cluster
	
	clusters = labels.tolist()

	file = open("FilteredResult.xml","w")
	file.write('<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>\n')
	file.write('<root>\n')
	tree = ET.parse('result.xml')
	root = tree.getroot()
	for child in root:
		for aux in child:
			if aux.text == 'Accidente':
				file.write('\t<Incidencia>\n')
				for aux in child:
					file.write("\t\t<"+str(aux.tag)+">"+str(aux.text)+"</"+str(aux.tag)+">\n")
				if labels.size > 0:
					file.write('\t\t<zona>'+str(clusters.pop())+'</zona>\n')
				file.write('\t</Incidencia>\n')
	file.write('</root>')
	file.close()


if __name__ == "__main__":
	main()