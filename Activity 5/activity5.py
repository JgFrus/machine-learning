import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import sklearn.cluster
import numpy 
from sklearn.decomposition import PCA
from sklearn import preprocessing


def main():
	#Process
	tree = ET.parse('DataCard.xml')
	root = tree.getroot()
	data=[]
	zones=[]
	valid=False
	for child in root:
		data = [int(aux.text) for aux in child]
		zones.append(data)	

	#1. Normalization of the data
	min_max_scaler = preprocessing.MinMaxScaler()
	zones = min_max_scaler.fit_transform(zones)

	#2. PCA Estimation
	estimator = PCA (n_components = 2)
	X_pca = estimator.fit_transform(zones)

	#3. Plot PCA
	numbers = numpy.arange(len(X_pca))

	fig, ax = plt.subplots()

	for i in range(len(X_pca)):
	    plt.text(X_pca[i][0], X_pca[i][1], numbers[i]) 
	   
	plt.xlim(-1, 4)
	plt.ylim(-0.2, 1)


	ax.grid(True)
	fig.tight_layout()

	plt.show()


if __name__ == "__main__":
	main()