import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import sklearn.cluster
import numpy 
from sklearn import preprocessing
from scipy import cluster


def main():
	#Process
	tree = ET.parse('DataCard.xml')
	root = tree.getroot()
	data=[]
	zones=[]
	valid=False
	for child in root:
		data = [int(aux.text) for aux in child]
		zones.append(data)	

	
	#1. Normalization of the data
	min_max_scaler = preprocessing.MinMaxScaler()
	zones = min_max_scaler.fit_transform(zones)

	# 2. Compute the similarity matrix
	euclidean_dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
	matsim = euclidean_dist.pairwise(zones)
	avSim = numpy.average(matsim)
	print("%s\t%6.2f" % ('Distancia Media', avSim))

	# 3. Building the Dendrogram	
	clusters = cluster.hierarchy.linkage(matsim, method = 'ward')
	cluster.hierarchy.dendrogram(clusters, color_threshold=3)
	plt.show()

	clusters = cluster.hierarchy.fcluster(clusters, 3, criterion = 'distance')
	print(clusters)
	i=0

	for child in root:
		child.attrib['cluster']=clusters[i]
		i+=1

	#Calculate the media of accidents
	dic_media_1={}
	dic_media_2={}
	n1=0
	n2=0
	for child in root:
		if child.attrib['cluster']==1:
			n1+=1
			for aux in child:
				if aux.tag in dic_media_1.keys():
					dic_media_1[aux.tag]+=int(aux.text)
				else:
					dic_media_1[aux.tag]=int(aux.text)

		if child.attrib['cluster']==2:
			n2+=1
			for aux in child:
				if aux.tag in dic_media_2.keys():
					dic_media_2[aux.tag]+=int(aux.text)
				else:
					dic_media_2[aux.tag]=int(aux.text)

	for clave, valor in dic_media_1.items():
		dic_media_1[clave]=dic_media_1[clave]/n1

	for clave, valor in dic_media_2.items():
		dic_media_2[clave]=dic_media_2[clave]/n2

	print(dic_media_1)
	print(dic_media_2)


if __name__ == "__main__":
	main()