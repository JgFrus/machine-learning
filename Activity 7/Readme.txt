We choose the diabetes dataset. On this problem, the decision tree is for detecting the diabetes with attributes like the skin, insuline, mass...

We execute  6 differents algorithms:
-J48
-DecisionStump
-HoeffdingTree
-LMT
-RandomTree
-REPTree

We choose the J48 as the best tree because it classifies the 73.8281% of the instances correctly and it's the smallest tree.
Other option could be the LMT tree, it has a 77.474% of correctly classified instances, but the J48 tree is smaller than the LMT, that's why whe choose the J48 as the best option.

The LMT has a 77.474% of correctly classified instances, but it has a logical tree that is not clear, at least for the knowledge we have at the moment. Also, the HoeffdingTree has a 76.1719% of correctly classified instances, but the tree generated has only one node, and classifies using one attribute, plas <= 17.909 for negative diabetes and > 17.909 for positive.
