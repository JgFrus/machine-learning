import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
 
import sklearn.cluster
from sklearn import metrics
from sklearn import neighbors
import numpy as np

import random as r

def main():
	#Process
	tree = ET.parse('TrainingData.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	zonas=[]
	for child in root:
		longitudes.append(float(child[12].text))
		latitudes.append(float(child[13].text))
		zonas.append(int(child[14].text))

	data=[]
	for i in zip(longitudes,latitudes,zonas):
		data.append(i)
	
	#Training and Test data
	training=[]
	test=[]

	for aux in data:
		if r.random()>0.6:
			test.append(aux)
		else:
			training.append(aux)

	#Training
	TrainingX=[] #Longitud, Latitud
	TrainingY=[] #zona
	for aux in training:
		TrainingX.append([aux[0],aux[1]])
		TrainingY.append(aux[2])
	
	#Test
	TestX=[]
	TestY=[]
	for aux in test:
		TestX.append([aux[0],aux[1]])
		TestY.append(aux[2])


	percent=[]
	for weights in ['uniform', 'distance']:
		for k in range(1,15):
			clf = neighbors.KNeighborsClassifier(k, weights=weights)
			clf.fit(TrainingX, TrainingY)
			Z = clf.predict(TrainingX)
			score = clf.score(TrainingX,TrainingY)
			percent.append([k,score])

	#Show the better K-value
	k_value=0
	aux=0
	for k,value in percent:
		if value>aux:
			aux=value
			k_value=k
	print(k_value,aux)
	
	#Now we have the better value for k, execute de KNN again
	#New Data
	tree = ET.parse('IncidenciasTDTHist.xml')
	root = tree.getroot()
	longitudes = []
	latitudes = []
	for child in root:
		if child[0].text == "Obras":
			if child[12].text != None and child[13].text != None:
				longitudes.append(float(child[12].text))
				latitudes.append(float(child[13].text))
	data=[]
	for i  in zip(longitudes,latitudes):
		data.append(i)
				
	clf = neighbors.KNeighborsClassifier(k, weights=weights)
	clf.fit(TestX, TestY)
	work_zones = clf.predict(data)
	print(work_zones,len(work_zones))
	#Write a file with the work and the zone
	f = open ("Result.txt", "a")
	work=0
	for zone in work_zones:
		f.write("Work: "+str(work)+" -> zone: "+str(zone)+"\n")
		work=work+1
	f.close()
	plt.plot(work_zones)
	plt.xlabel('Work Zones')
	fig1 = plt.gcf()
	plt.show()
	fig1.savefig('Result.png', dpi=100)

if __name__ == "__main__":
	main()
